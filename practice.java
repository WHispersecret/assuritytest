package restassured;



import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Assert;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.specification.RequestSpecification;

 
public class practice{
 
	@Test
	public void GetDetails()
	{   
		// Specifying the base URL
		RestAssured.baseURI = "https://api.tmsandbox.co.nz/v1/Categories/6328/Details.json?catalogue=false";
 
		// Get the RequestSpecification of the request that you want to sent
		// to the server. The server is specified by the BaseURI that we have
		// specified in the above step.
		RequestSpecification httpRequest = RestAssured.given();
 
		// Make a request to the server by specifying the method Type and the method URL.
		// This will return the Response from the server. Store the response in a variable.
		Response response = httpRequest.request(Method.GET);
		JsonPath jsonPathEvaluator = response.jsonPath();
		// Now let us print the body of the message to see what response
		// we have recieved from the server
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		
		
		
		String name = jsonPathEvaluator.get("Name");
		System.out.println("Name received from Response " + name);
		
		// Validate the response
		//Assert.assertEquals(name, "Badges");
		try {
		    Assert.assertEquals(name, "Badges");
		} catch (AssertionError e) {
		    System.out.println("Test Case 1 failed");
		    throw e;
		}
		System.out.println("Test Case 1 passed");
		
		Boolean CanListClassifieds = jsonPathEvaluator.get("CanListClassifieds");
		System.out.println("Boolean received from Response " + CanListClassifieds);
		try {
		    Assert.assertEquals(CanListClassifieds, false);
		} catch (AssertionError e) {
		    System.out.println("Test Case 2 failed");
		    throw e;
		}
		System.out.println("Test Case 2 passed");
		
		ArrayList charities = jsonPathEvaluator.get("Charities");
		//System.out.println(charities);
		for (int i = 0; i < charities.size(); i++) {
			//System.out.println(charities.get(i));
		
			if (((HashMap) charities.get(i)).containsKey("Description")) {
				if (((HashMap) charities.get(i)).containsKey("Tagline")) {
				Object value = ((HashMap) charities.get(i)).get("Description");
				Object value2 = ((HashMap) charities.get(i)).get("Tagline");
				 
				 if (value.equals("Plunket")) {

					 System.out.println("There is a Charity with the Description = 'Plunket'");
					 if (((String) value2).contains("well child health services")){
						 System.out.println("Plunket has a Tagline which contains the text: 'well child health services'. ");
						 System.out.println("test 3 passed");
					 }else {
						 System.out.println("Plunket does not have a Tagline which contains the text: 'well child health services'. ");
						 System.out.println("test 3 failed");
					 }
						 
					 }
				 
					 
					
		
					 
				 }
		
	           
	           
	        }

			
			
			
			
			 
		}
			
		
		
	
 
	
 
}
}
